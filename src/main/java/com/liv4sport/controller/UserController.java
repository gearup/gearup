package com.liv4sport.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.liv4sport.user.User;

/**
 * Handles requests for the Employee service.
 */
@Controller
public class UserController {

    private static final Logger logger = LoggerFactory
	    .getLogger(UserController.class);

    // Map to store employees, ideally we should use database
    Map<Integer, User> empData = new HashMap<Integer, User>();

    @RequestMapping(value = UserRestURIConstants.DUMMY_EMP, method = RequestMethod.GET)
    public @ResponseBody User getDummyEmployee() {
	logger.info("Start getDummyEmployee");
	User emp = new User();
	emp.setId(9999);
	emp.setName("Dummy");
	emp.setCreatedDate(new Date());
	empData.put(9999, emp);
	return emp;
    }

    @RequestMapping(value = UserRestURIConstants.GET_EMP, method = RequestMethod.GET)
    public @ResponseBody User getEmployee(@PathVariable("id") int empId) {
	logger.info("Start getEmployee. ID=" + empId);
	return empData.get(empId);
    }

    @RequestMapping(value = UserRestURIConstants.GET_ALL_EMP, method = RequestMethod.GET)
    public @ResponseBody List<User> getAllEmployees() {
	logger.info("Start getAllEmployees.");
	List<User> emps = new ArrayList<User>();
	Set<Integer> empIdKeys = empData.keySet();
	for (Integer i : empIdKeys) {
	    emps.add(empData.get(i));
	}
	return emps;
    }

    @RequestMapping(value = UserRestURIConstants.CREATE_EMP, method = RequestMethod.POST)
    public @ResponseBody User createEmployee(@RequestBody User emp) {
	logger.info("Start createEmployee.");
	emp.setCreatedDate(new Date());
	empData.put(emp.getId(), emp);
	return emp;
    }

    @RequestMapping(value = UserRestURIConstants.DELETE_EMP, method = RequestMethod.PUT)
    public @ResponseBody User deleteEmployee(@PathVariable("id") int empId) {
	logger.info("Start deleteEmployee.");
	User emp = empData.get(empId);
	empData.remove(empId);
	return emp;
    }

}
