package com.liv4sport.user;

public class Name {
	
	private String firstName;
	private String lastName;
	private String nickName;
	
	public Name (){
		
	}
	
	public Name(String firstName, String lastName, String nickName){
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickName = nickName;
				
	}
	
	public Name (Name name){
		this.firstName = name.getFirstName();
		this.lastName = name.getLastName();
		this.nickName = name.getNickName();
	}
	
	public Name (String fullName){
		if (fullName.contains(" ")){
			
		}
		else{
			this.firstName = fullName;
			this.lastName = null;
		}
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String getFullName(){
		return firstName + " " + lastName; 
	}

	
	
}
