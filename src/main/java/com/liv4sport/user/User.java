package com.liv4sport.user;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;

public class User implements Serializable {

    private static final long serialVersionUID = -7788619177798333712L;

    private int id;
    private Date createdDate;

    private String emailAddress;
    private Name name;
    private Integer age;
    private String hashedPassword;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    @JsonSerialize(using = DateSerializer.class)
    public Date getCreatedDate() {
	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
    }

	public void setName(Name name) {
		this.name = new Name(name);
	}

	public Name getName() {
		return this.name;
	}

}
